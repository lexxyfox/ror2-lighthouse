#!
dl_bepinex () {
  curl -Lsf --compressed "$(
    curl -Lsf --compressed \
      "https://api.github.com/repos/BepInEx/$1/releases/latest" \
      | jq -r .assets"$2".browser_download_url
  )" | busybox unzip -
}

compile () {
  #DOTNET_CLI_TELEMETRY_OPTOUT=1 DOTNET_NOLOGO=1 \
  #  dotnet build "$1"                  \
  msbuild -restore "$1"                \
    -p:Configuration=Release           \
    -p:DebugSymbols=false              \
    -p:DebugType=None                  \
    -p:Deterministic=true              \
    -p:GenerateDependencyFile=false    \
    -p:GenerateDocumentationFile=false \
    -p:Optimize=true                   \
    -p:OutDir="../BepInEx/$2" < /dev/null
}

set -euxo pipefail - https://github.com/risk-of-thunder/R2API/trunk
svn export $1/R2API         &
svn export $1/R2API.MonoMod &
wait

mkdir BepInEx
unxz < patch | patch -p0 --no-backup-if-mismatch
(convert -resize 256x256 icon.svgz tmp.png && \
 pngcrush -brute -reduce -rem allb tmp.png icon.png) &
cat <<< "$(jq -S '.version_number = (now | gmtime | strftime("%-y.%-m.1%d%H"))' \
  < manifest.json)" > manifest.json &

dl_bepinex BepInEx.MonoMod.Loader [0]                               &
dl_bepinex BepInEx '[] | select(.name | startswith("BepInEx_x64"))' &
wait

compile R2API         plugins
compile R2API.MonoMod monomod
rm BepInEx/*/*.xml *.txt

