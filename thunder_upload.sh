#!

curl2 () {
  curl -Lsf --compressed -b "$COOKIE_JAR" -c "$COOKIE_JAR" "$@"
}

curl_parse () {
  xsltproc --html --nonet - <( curl2 "$@" ) << EOF 2> /dev/null
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:str="http://exslt.org/strings">
  <xsl:output method="text" />
$(cat)
  <xsl:template match="text()"/>
</xsl:stylesheet>
EOF
}

: "${THUNDERSTORE_URL:=https://beta.thunderstore.io}"

echo
COOKIE_JAR="$(mktemp)"
trap "rm -f \"$COOKIE_JAR\"" EXIT

echo '[1/6] Opening GitHub session...' 1>&2
login_data="$(curl_parse "$THUNDERSTORE_URL/auth/login/github" <<- 'EOF'
  <xsl:template match="//*[@id='login']/form//input[@value]">
    <xsl:copy-of select="str:encode-uri(@name, 'true')"/>
    <xsl:text>=</xsl:text> 
    <xsl:copy-of select="str:encode-uri(@value, 'true')"/>
    <xsl:text>&amp;</xsl:text> 
  </xsl:template>
EOF
)"

echo '[2/6] Authenticating with GitHub...' 1>&2
# TODO: remove fixed github.com URL
mfa_data="$(curl_parse 'https://github.com/session' \
  --data-raw "$login_data" \
  --data-urlencode "login=$GITHUB_UN" \
  --data-urlencode "password=$GITHUB_PW" \
  <<- 'EOF'
  <xsl:template match="//form[@action='/sessions/verified-device']//input[@value != '']">
    <xsl:copy-of select="str:encode-uri(@name, 'true')"/>
    <xsl:text>=</xsl:text> 
    <xsl:copy-of select="str:encode-uri(@value, 'true')"/>
    <xsl:text>&amp;</xsl:text> 
  </xsl:template>
EOF
)"

if [ ! -z "$mfa_data" ]; then
  echo -n '[3/6]   Fetching 2FA code... ' 1>&2
  sleep 5
  
  mfa_code="$(curl2 "$GITHUB_2F" \
    | jq -r '.[-1].text' \
    | sed -n 's/Verification code: //p')"
  echo "$mfa_code"
  
  echo '[4/6]   Performing 2FA...' 1>&2
  curl2 'https://github.com/sessions/verified-device' \
    --data-raw "$mfa_data" \
    --data-urlencode "otp=$mfa_code" \
    > /dev/null
fi

echo '[5/6] Preparing Thunderstore upload...' 1>&2
csrf_token="$(curl_parse "$THUNDERSTORE_URL/package/create/" <<- 'EOF'
  <xsl:template match="//input[@name='csrfmiddlewaretoken']">
    <xsl:copy-of select="string(@value)"/>
  </xsl:template>
EOF
)"

echo '[6/6] Uploading file to Thunderstore...' 1>&2
curl2 "$THUNDERSTORE_URL/package/create/" \
  --header "Referer: $THUNDERSTORE_URL/package/create/" \
  --form-string "csrfmiddlewaretoken=$csrf_token" \
  --form 'file=@-' \
  > /dev/null


