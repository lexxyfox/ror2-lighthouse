Hlep, readme needs improvement.

### Wat's dis?
It's a combination of the latest BepInEx and R2API for easy installation, automatically and frequently updated to allow playing with the latest mods. Just drop into your Risk of Rain 2 folder, ensure winhttp.dll is in your load path, and drop in whatever mods you'd like from the Thunderstore into the BepInEx/plugins folder.

I created this mod[pack] so my friends and I could play together, and am sharing with you in the hopes that you might like it too. If you do like it, that's great! If you don't like, that's fine too, just let me know if this is taking up too much space on your website. Please note that this modpack has no official affiliation with BepInEx, R2API, or Risk of Rain; just with me.

### What's included?

* [R2API](https://github.com/risk-of-thunder/R2API)
* [BepInExPack](https://thunderstore.io/package/bbepis/BepInExPack)
    * [BepInEx MonoMod Loader](https://github.com/BepInEx/BepInEx.MonoMod.Loader)
    * [BepInEx](https://github.com/BepInEx/BepInEx)
        * [Unity Doorstop](https://github.com/NeighTools/UnityDoorstop)
        * [HarmonyX](https://github.com/BepInEx/HarmonyX)
        * [Mono Mod](https://github.com/MonoMod/MonoMod)
        * [Cecil](https://github.com/jbevain/cecil)

Please go and support the 100+ developers that made the entire RoR2 modding community possible!

This pack also obsoletes these mods:

* [UnmoddedVersion](https://thunderstore.io/package/frostycpu/UnmoddedVersion)
* [SteamBuildID](https://thunderstore.io/package/baekhorangi/SteamBuildID)
* [SetBuildID](https://thunderstore.io/package/TheRealElysium/SetBuildID)
